from ultralytics import YOLO
import cv2
import numpy as np
from matplotlib import pyplot as plt

class Keypoint():
    x_pos: float
    y_pos: float

model = YOLO('yolov8n-pose.pt')

# Use webcam
video_path = 0
cap = cv2.VideoCapture(video_path)

success, frame = cap.read()

# Run pose estimation on first frame
results = model(frame, save=True)
results_keypoints = results[0].keypoints.xyn.cpu().numpy()[0]

cap.release

print(results_keypoints)

# Set the figure size
plt.rcParams["figure.figsize"] = [7.50, 3.50]
plt.xlim(0, 1)
plt.ylim(1, 0)

# Scatter plot
plt.scatter(results_keypoints[:, 0], results_keypoints[:, 1], c=results_keypoints[:, 1])

# Display the plot
plt.show()