from ultralytics import YOLO
import cv2
import socket
from datetime import datetime

model = YOLO('yolov8n-pose.pt')

host, port = "127.0.0.1", 25001

# SOCK_STREAM means TCP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

video_path = 0
cap = cv2.VideoCapture(video_path)

try:
    # Connect to the server and send the data
    sock.connect((host, port))
    #response = sock.recv(1024).decode("utf-8")
    #print (response)

    while cap.isOpened():
        success, frame = cap.read()

        if success:
            results = model(frame, save=False)
            results_keypoints = results[0].keypoints.xyn.cpu().numpy()[0]
            data = str(1 - results_keypoints[10,0]) + ',' + str(1 - results_keypoints[10,1]) + ',0' #results_keypoints[10,#] is right wrist
            print(datetime.now())

            sock.sendall(data.encode("utf-8"))
            print('data sent:' + data)

        else:
            break

except:
    print('could not connect')

cap.release