from ultralytics import YOLO
import cv2

model = YOLO('yolov8n-pose.pt')

#for video or photo
#source = "D:\YOLO\sonny in motion.mp4"
#model.predict(source, save=True, imgsz=320, conf=0.5)

video_path = 0
cap = cv2.VideoCapture(video_path)

while cap.isOpened():
    success, frame = cap.read()

    if success:
        results = model(frame, save=True)

        annotated_frame = results[0].plot()

        cv2.imshow("YOLOv8 Inference", annotated_frame)

        if cv2.waitKey(1) & 0xFF == ord("q"):
            break

    else:
        break

cap.release