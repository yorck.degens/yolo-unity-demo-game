using System.Net;
using System.Net.Sockets;
using System.Text;
using UnityEngine;
using System.Threading;
using System.Globalization;
using System.Diagnostics;

public class MotionControlScript : MonoBehaviour
{
    Thread thread;
    public int connectionPort = 25001;
    TcpListener server;
    TcpClient client;
    bool running = true;
    // Position is the data being received in this example
    Vector3 position = new Vector3(8,4,0);
    public Rigidbody2D swordBody;
    public float speed = 20;


    void Start()
    {
        //Run the Python YOLO script
        //Process.Start("YoloPoseClient.exe");

        // Receive on a separate thread so Unity doesn't freeze waiting for data
        ThreadStart ts = new ThreadStart(GetData);
        thread = new Thread(ts);
        thread.Start();
    }

    void GetData()
    {
        // Create the server
        server = new TcpListener(IPAddress.Any, connectionPort);
        server.Start();

        // Create a client to get the data stream
        client = server.AcceptTcpClient();

        // Start listening
        while (running)
        {
            Connection();
        }
        client.GetStream().Close();
        server.Stop();
    }

    void Connection()
    {
        NetworkStream nwStream = client.GetStream();
        byte[] buffer = new byte[client.ReceiveBufferSize];

        // Read the bytes and decode into a string
        int bytesRead = nwStream.Read(buffer, 0, client.ReceiveBufferSize);
        string dataReceived = Encoding.UTF8.GetString(buffer, 0, bytesRead);

        // Make sure we're not getting an empty string
        //dataReceived.Trim();
        if (dataReceived != null && dataReceived != "")
        {
            // Convert the received string of data to the format we are using
            position = ParseData(dataReceived);
        }
    }

    // Use-case specific function, need to re-write this to interpret whatever data is being sent
    public static Vector3 ParseData(string dataString)
    {
        UnityEngine.Debug.Log(dataString);
        // Remove the parentheses
        if (dataString.StartsWith("(") && dataString.EndsWith(")"))
        {
            dataString = dataString.Substring(1, dataString.Length - 2);
        }

        // Split the elements into an array
        string[] stringArray = dataString.Split(',');

        // Store as a Vector3
        Vector3 result = new Vector3(
            float.Parse(stringArray[0], CultureInfo.InvariantCulture.NumberFormat) * 16,
            float.Parse(stringArray[1], CultureInfo.InvariantCulture.NumberFormat) * 8,
            float.Parse(stringArray[2], CultureInfo.InvariantCulture.NumberFormat));

        return result;
    }

    public void StopConnection()
    {
        running = false;
    }

    // Position is the data being received in this example
    // Vector3 position = Vector3.zero;

    void Update()
    {
        // Set this object's position in the scene according to the position received
        // swordBody.velocity = position;
        transform.position = Vector3.MoveTowards(transform.position, position, speed * Time.deltaTime);
    }
}