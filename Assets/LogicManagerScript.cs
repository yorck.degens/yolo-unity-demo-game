using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LogicManagerScript : MonoBehaviour
{
    public int playerScore;
    public Text scoreText;
    public AudioSource scoreSound;
    public MotionControlScript motion;

    private void Start()
    {
        motion = GameObject.FindGameObjectWithTag("Motion").GetComponent<MotionControlScript>();
    }

    [ContextMenu("Increase Score")]
    public void AddScore(int scoreToAdd)
    {
        playerScore += scoreToAdd;
        scoreText.text = playerScore.ToString();

        // For testing
        //if (playerScore >= 2)
        //{
        //    QuitGame();
        //}
    }
    
    public void QuitGame()
    {
        motion.StopConnection();
        Application.Quit();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) == true)
        {
            QuitGame();
        }
    }
}
