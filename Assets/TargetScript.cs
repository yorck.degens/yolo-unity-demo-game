using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetScript : MonoBehaviour
{
    private int[,] positions = { { 15, 7 }, { 15, 1 }, { 1, 7 }, { 1, 1 } };
    public int scorecounter = 0;
    public LogicManagerScript logic;

    void Start()
    {
        logic = GameObject.FindGameObjectWithTag("Logic").GetComponent<LogicManagerScript>();

        int randomInt = Random.Range(0, 4);
        transform.position = new Vector3(positions[randomInt, 0], positions[randomInt, 1], 0);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        logic.AddScore(1);
        int randomInt = Random.Range(0, 4);
        transform.position = new Vector3(positions[randomInt, 0], positions[randomInt, 1], 0);
    }
}
